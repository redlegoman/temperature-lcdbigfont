/*
 *
 *	This program assumes the following:
 *
 *
 *	For 4-bit interface:
 *		GPIO 4-7 is connected to display data pins 4-7.
 *		GPIO 11 is the RS pin.
 *		GPIO 10 is the Strobe/E pin.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <unistd.h>
#include <string.h>
#include <time.h>

#include <wiringPi.h>
#include <lcd.h>

#ifndef	TRUE
#  define	TRUE	(1==1)
#  define	FALSE	(1==2)
#endif

// Global lcd handle:
static int lcdHandle ;

/*
 * usage:
 */

int usage (const char *progName)
{
  fprintf (stderr, "Usage: %s line message\n", progName) ;
  return EXIT_FAILURE ;
}

static const char *message = "";


/* **********************************************************************************/
/* This section defines the characters to place into the HD44780's GCRAM. It can hold 8 
   user-defined caharacters 
*/
static unsigned char CharStop [8] = 
{
  0b00110,
  0b01111,
  0b01111,
  0b00110,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
} ;
static unsigned char CharDegree [8] = 
{
  0b00110,
  0b01001,
  0b01001,
  0b00110,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
} ;
static unsigned char CharBR [8] = 
{
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b00011,
	0b01111,
	0b01111,
	0b11111,
} ;
static unsigned char CharBB [8] = 
{
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b11111,
	0b11111,
	0b11111,
	0b11111,
} ;
static unsigned char CharBL [8] = 
{
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b11000,
	0b11110,
	0b11110,
	0b11111,
} ;
static unsigned char CharTL [8] = 
{
	0b11111,
	0b11110,
	0b11110,
	0b11000,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
} ;
static unsigned char CharTT [8] = 
{
	0b11111,
	0b11111,
	0b11111,
	0b11111,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
} ;
static unsigned char CharTR [8] = 
{
	0b11111,
	0b01111,
	0b01111,
	0b00011,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
} ;
static unsigned char CharDOT [8] = 
{
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b01100,
	0b01100,
	0b00000,
} ;

/* **********************************************************************************/
//--------------------------------------
int BIG(int add, int numpos, int numToPrint){
	numpos = (numpos * 4) +1; // Position the character on the LCD screen. 1 added to shift it to the end.

/*
These next lines define the large numbers from 0 to 9. The numbers in the array refer to the custom characters and their
location in the CGRAM on the HD44780. 255 = solid block, 1= bottom right block character defined above.
*/
int big4_1[] = {1,2,3,0,       2,3,254,0,     1,2,3,0,       1,2,3,0,       2,254,254,0,   2,2,2,0,       1,2,3,0,       2,2,2,0,       1,2,3,0,       1,2,3,0,       254,254,254,254,};
int big4_2[] = {255,254,255,0, 254,255,254,0, 1,2,255,0,     254,2,255,0,   255,2,2,0,     255,2,2,0,     255,2,3,0,     254,2,255,0,   255,2,255,0,   255,254,255,0, 2,2,2,0};
int big4_3[] = {255,254,255,0, 254,255,254,0, 255,254,254,0, 254,254,255,0, 254,255,254,0, 254,254,255,0, 255,254,255,0, 254,255,254,0, 255,254,255,0, 4,6,255,0,     254,254,254,254};
int big4_4[] = {4,6,5,0,       6,6,6,0,       4,6,6,0,       4,6,5,0,       254,6,254,0,   6,6,5,0,       4,6,5,0,       254,6,254,0,   4,6,5,0,       254,254,6,0,    254,254,254,254};
	
// the number to screen at the position specified.
 lcdPosition(lcdHandle, (0 + numpos) + add ,0);	 // LINE 1
 lcdPutchar (lcdHandle, big4_1[(numToPrint * 4) + 0 ]);
 lcdPutchar (lcdHandle, big4_1[(numToPrint * 4) + 1 ]);
 lcdPutchar (lcdHandle, big4_1[(numToPrint * 4) + 2 ]);
	
 lcdPosition(lcdHandle, (0 + numpos) + add,1);	 //LINE 2
 lcdPutchar (lcdHandle, big4_2[(numToPrint * 4) + 0 ]);
 lcdPutchar (lcdHandle, big4_2[(numToPrint * 4) + 1 ]);
 lcdPutchar (lcdHandle, big4_2[(numToPrint * 4) + 2 ]);
	
 lcdPosition(lcdHandle, (0 + numpos) + add,2);	 //LINE 3
 lcdPutchar (lcdHandle, big4_3[(numToPrint * 4) + 0 ]);
 lcdPutchar (lcdHandle, big4_3[(numToPrint * 4) + 1 ]);
 lcdPutchar (lcdHandle, big4_3[(numToPrint * 4) + 2 ]);

 lcdPosition(lcdHandle, (0 + numpos) + add,3);	 //LINE 4
 lcdPutchar (lcdHandle, big4_4[(numToPrint * 4) + 0 ]);
 lcdPutchar (lcdHandle, big4_4[(numToPrint * 4) + 1 ]);
 lcdPutchar (lcdHandle, big4_4[(numToPrint * 4) + 2 ]);

return 0;
}

int main (int argc, char *argv[])
{
  int bits, rows, cols ;
  //if (argc != 2)
   // return usage (argv [0]) ;
  bits = 4 ;
	cols = 20 ;
  rows = 4 ;
	message = argv[2];

  wiringPiSetup () ; // Initialise WiringPi (http://wiringpi.com/)

  if (bits == 4)
    lcdHandle = lcdInit (rows, cols, 4, 11,10, 0,1,2,3,0,0,0,0) ;

  if (lcdHandle < 0)
  {
    fprintf (stderr, "%s: lcdInit failed\n", argv [0]) ;
    return -1 ;
  }

  //lcdClear       (lcdHandle) ;

	lcdPosition (lcdHandle, 0, 0) ; 
	lcdPuts (lcdHandle, message) ; // put the message (2nd argument) at position 0,0 (top left)

  // Load all the custom characters into the CGRAM on the HD44780:
	lcdCharDef  (lcdHandle, 0, CharDegree) ; 
	lcdCharDef  (lcdHandle, 1, CharBR) ; 
	lcdCharDef  (lcdHandle, 2, CharBB) ; 
	lcdCharDef  (lcdHandle, 3, CharBL) ; 
	lcdCharDef  (lcdHandle, 5, CharTL) ; 
	lcdCharDef  (lcdHandle, 6, CharTT) ; 
	lcdCharDef  (lcdHandle, 4, CharTR) ; 
	lcdCharDef  (lcdHandle, 7, CharDOT) ;
	lcdCharDef  (lcdHandle, 8, CharStop) ;
	lcdPosition (lcdHandle, 0, 0) ; 

	int x = 0; // to contain the number
	int i; // for the for loop
	char celsius[20] = "000.0"; // initialise var to 00.0
	strcpy (celsius, argv[1]); // copy command line argument 1 to the celsius var
	
	int len = strlen(celsius);// how many characters in the "celsius" string sent to the program
	int posoffset = 1; //add 1 to the position of the printed characters
	int dotprinted = 0; // have we printed the "." yet?
	int add = 2; 	// add extra to the printing position before the "." is printed
								// this is a cludgy way of making the decimal point take up less space
	int pos = 0 ; // increment as we loop throught the string
	// loop through each character in the string
	if(len == 3) { posoffset = 2;}
	//BIG(add,0,10);
	for (i = 0; i < len; i++)
	{
		if(dotprinted == 0){ add = 2 ; }else{add = 0; } // if we haven't printed the ".", add 2
																										// position of the printed characters
		x = celsius[i] - '0';
													//check to see if the char is a "." If it isn't, pass it to the BIG function to
		if(celsius[i] != '.')	// print at position i + posoffset on the LCD
		{
			if(celsius[i] == '-')	
			{
				BIG(add,i + posoffset,10);
			}
			else {
			BIG(add,i + posoffset,x);
			}
		} 
		else 
		{ 
			dotprinted = 1; // if we're here, then the character is "." so, set the variable to tell us this.
			lcdPosition(lcdHandle,((i+posoffset)*4) + 3,3); // only works for a 20x4 LCD.
			lcdPutchar(lcdHandle,8); // print the custom decimal point (charStop) character after the previous number
		}
		pos++;
		
	}



  return 0 ;
}
