Display temperature on 20x4 HD44780 LCD connected to Raspberry Pi.
Uses a large custom font for the main temperature display. 
Format of number needs to be "99.9"
