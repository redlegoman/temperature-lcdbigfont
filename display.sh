#!/bin/bash
# script to grab temperature reading from a remote webserver (outside)
# and from a DS18B20 attached locally and feed them one after the other
# to my C program for displaying them in a big font on a 20x4 HD44780 LCD
#
DIR=/usr/local/sbin
while true 
do
	TMP=/tmp/lcd2
	LOG=/dev/null
	DDIR=/sys/bus/w1/devices
	DALLASES="$DDIR/28-00000457657b/w1_slave"
	NUM=1
	for DALLAS in $DALLASES
	do
		echo "-------- $NUM ------------ $DALLAS "
		CRC="NO"
		while [[ $CRC == "NO" ]]
		do
			cat $DALLAS > $TMP
			cat $TMP >> $LOG
			CRC=$(cat $TMP|grep crc|awk '{print $12}')
			GOTDALLASINSIDE=$(cat $TMP | tail -1|awk -F"=" '{print $2}')
			GOTDALLASINSIDE=$(echo "scale=1 ; $GOTDALLASINSIDE/ 1000.00"|/usr/bin/bc)
			TEMP[${NUM}]=$GOTDALLASINSIDE
		done
		NUM=$(expr $NUM + 1 )
	done
	echo "TEMP1 = ${TEMP[1]} , TEMP2 = ${TEMP[2]}"
	$DIR/big ${TEMP[1]} in
	sleep 2
	$DIR/big $(/usr/bin/curl -s http://10.10.10.80/temps/out.php) out
	sleep 2
done
